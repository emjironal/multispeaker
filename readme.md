# Multispeaker

## Objective

To create a program that runs in background, like a new audio playback, that allows assigning ranges of frequencies to different audio output sources (speakers)

## First steps

- [ ] Investigate audio features required to extract
    - [ ] Frequency
- [ ] Investigate audio library tools
    - [ ] pyAudioAnalysis
- [ ] Investigate how to use multiple audio sources at once on Linux
